---
title: Visualizing Expiring Ebay Book Auctions with Three.js
author: spencer-apple
template: article.jade
externalUrl: http://splayemu.gitlab.io/CMPS179_EbayVisual2/
withImage: expiringBookAuctions.png
date: 2013-05-15 15:00
---

Visualizes the 100 oldest book auctions on Ebay. 
Focuses on the realtime activity of Ebay through the growing of colorful flowers (pillars). 
Made for a Data Visualization class at UCSC. 
Code is <a href="https://gitlab.com/splayemu/CMPS179_EbayVisual2" target="_blank">here</a>.
