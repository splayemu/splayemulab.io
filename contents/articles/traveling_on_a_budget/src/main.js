
function parseNumber (numberString) {
    if (numberString === undefined) {
        return null;
    }
    if (! parseInt(numberString[0], 10)) {
        numberString = numberString.substring(1);
    }
    var number = parseFloat(numberString, 10);

    return isNaN(number) ? null : number;
};

function type (d) {
    d.date = new Date(d.date);
    d.amount = parseNumber(d.amount);
    return d;
}

function daysElapsed (start, end) {
    return Math.floor((end - start) / (1000*60*60*24));
}

// use dispatch.on("load.namespace", function (data) { ... }); to use the data from budget.csv
var dispatch = d3.dispatch("loaded"),
    twoDecimalRound = d3.format("$,.2f"),
    alternateCountryNames = {
        'CostaRica': 'Costa Rica'
    };

var countryColors = countries.reduce(function (previousValue, currentValue, currentIndex, array) {
    previousValue[currentValue.name] = d3.schemeCategory20[currentIndex];
    return previousValue;
}, {});

// calculate length of stay in each country
countries.forEach(function (country) {
    country.startAndEnds.forEach(function (dates) {
        dates.start = new Date(dates.start);
        dates.end = new Date(dates.end);
        dates.daysElapsed = daysElapsed(dates.start, dates.end);
    });

    var totalDays = country.startAndEnds.reduce(function (prev, curr) {
        return prev + curr.daysElapsed;
    }, 0);

    country.days = totalDays;
});

// total length (remember to add an extra day to the last place)
var totalDays = countries.reduce(function (prev, curr) {
    return prev + curr.days;
}, 0);

// update places to have a country tag
var re = /#(\w+)_/;
PLACES = PLACES.map(function (place) {
    place.country = re.exec(place.id)[1];

    if (alternateCountryNames.hasOwnProperty(place.country)) {
        place.country = alternateCountryNames[place.country];
    }

    return place;
});


document.addEventListener("DOMContentLoaded", function () {

    d3.csv("data/budget.csv", type, function (error, data) {
        if(error) throw error;

        // aggregate amount spent by date
        var byDate = d3.nest()
            .key(function (d) { return d.date; })
            .rollup(function (v) { return {
                amount: d3.sum(v, function (d) { return d.amount; }),
            }; })
            .entries(data);

        // aggregate amount spent and num days by place
        PLACES.forEach(function (place) {
            var days = [],
                endDate = new Date(place.endDate),
                startDate = new Date(place.startDate),
                difference = daysElapsed(startDate, endDate);

            place.midDate = new Date(((endDate - startDate) / 2) + startDate.getTime());

            byDate.forEach(function (day) {
                if(new Date(day.key) < endDate &&
                   new Date(day.key) >= startDate) {
                    days.push(day);
                }
            });

            daysNest = d3.nest()
                .rollup(function (v) { return {
                    'days': v.length,
                    'amount': d3.sum(v, function (d) { return d.value.amount; })
                }; })
                .entries(days);

            // covers empty days that we didn't enter any money for
            if (difference !== daysNest.days) {
                daysNest.days = difference;
            }

            place.days = days;
            place.value = daysNest;
        });

        // aggregate amount spent and num days by country
        var budgetPerCountry = d3.nest()
            .key(function(d) { return d.country; })
            .rollup(function (v) {
                var days = d3.sum(v, function (d) { return d.value.days; });
                var amount = d3.sum(v, function (d) { return d.value.amount; });
                return {
                    days: days,
                    amount: amount,
                    averageDailySpent: amount / days,
                };
            })
            .entries(PLACES);

        // understand el calafate and el chalten

        var newData = {
            data: data,
            byDate: byDate,
            budgetPerCountry: budgetPerCountry
        };

        dispatch.call("loaded", this, newData);
    });
});

dispatch.on("loaded.totalBudget", function (data) {
    var totalAverage = d3.nest()
        .rollup(function (v) {
            var sum = d3.sum(v, function (d) { return d.value.amount; });
            return {
                sum: sum,
                days: totalDays,
                averageDailySpent: sum / totalDays,
            }; })
        .entries(data.byDate);

    d3.select("#totalDays")
        .text(totalAverage.days);

    d3.select("#totalSpent")
        .text(twoDecimalRound(totalAverage.sum));

    d3.select("#averageDailySpent")
        .text(twoDecimalRound(totalAverage.averageDailySpent));

});

dispatch.on("loaded.budgetByCountry", function (data) {
    var barChart = graphComponents.barChart()
        .x(function (d) { return d.key; })
        .y(function (d) { return d.value.averageDailySpent; })
        .yTickFormat(d3.format("$,"))
        .yLabel('Average Daily Spending')
        .color(function (d) { return countryColors[d.key]; })
        .hoverText(function (d) { return twoDecimalRound(d.value.averageDailySpent); })
        .key(null);

    d3.select('#budgetByCountry')
        .datum(data.budgetPerCountry)
        .call(barChart);
});

dispatch.on("loaded.budgetByPlace", function (data) {
    var rScale = d3.scaleLinear()
        .domain(d3.extent(PLACES, function (d) { return d.value.days; }))
        .range([5, 15]);

    var scatterplotPlaces = graphComponents.scatterplot()
        .x(function (d) { return new Date(d.midDate); })
        .y(function (d) { return d.value.amount / d.value.days; })
        .r(function (d) { return rScale(d.value.days); })
        .xScaleGenerator(function (data, width, height) {
            return d3.scaleTime()
                .domain(d3.extent(data, function (d) { return new Date(d.midDate); }))
                .range([0, width])
                .nice(); })
        .xTickFormat(d3.timeFormat("%b '%y"))
        .yTickFormat(d3.format("$,"))
        .yLabel('Average Daily Spending')
        .color(function (d) { return countryColors[d.country]; })
        .hoverText(function (d) { return d.name;});

    d3.select('#spendingPerPlace')
        .datum(PLACES)
        .call(scatterplotPlaces);

    d3.selectAll('#spendingPerPlace .xAxis g text')
        .attr("transform", "rotate(30)")
        .attr("x", 3)
        .style("text-anchor", "start");

    var scatterplot1 = graphComponents.scatterplot()
        .x(function (d) { return d.value.days; })
        .y(function (d) { return d.value.amount / d.value.days; })
        .r(function (d) { return rScale(d.value.days); })
        .xScaleGenerator(function (data, width, height) {
            return d3.scaleLinear()
                .domain([0, d3.max(data, function (d) { return d.value.days; })])
                .range([0, width]); })
        .yTickFormat(d3.format("$,"))
        .xLabel('Length of Stay (days)')
        .yLabel('Average Daily Spending')
        .color(function (d) { return countryColors[d.country]; })
        .hoverText(function (d) { return d.name;});

    d3.select('#avgVsLength')
        .datum(PLACES)
        .call(scatterplot1);
});
