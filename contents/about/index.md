---
view: none
---

Located in the Bay Area, California. 

Enjoys nutrition (food), programming, reading, and traveling. 

Feel free to email him: ***spencertapple@gmail.com***

<a href="https://www.linkedin.com/in/spencerapple" target="_blank"><img src="/about/linkedin.svg" alt="linkedin" title="linkedin"></a>
<a href="https://github.com/splayemu" target="_blank"><img src="/about/github.svg" alt="github" title="github"></a>
